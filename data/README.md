# Data

Files:

```
├── EDEN & PELAGIE - Dates
│   ├── EDEN_dates_01062020.rds
│   ├── PELAGIE_gro_dates_01062020.rds
│   ├── PELAGIE_Q2_dates_01062020.rds
│   └── PELAGIE_Q6_dates_01062020.rds
│
├── expos
│   │
│   ├── Expo_prenat_EDEN
│   │   ├── expo_prenat_EDEN_temp1km_03062020.rds
│   │   └── expo_prenat_EDEN_temp200m_03062020.rds
│   │
│   ├── Expo_prenat_PELAGIE
│   │   ├── expo_prenat_PELAGIE_gro_temp1km_03062020.rds
│   │   ├── expo_prenat_PELAGIE_gro_temp200m_03062020.rds
│   │   ├── expo_prenat_PELAGIE_Q2_temp1km_03062020.rds
│   │   ├── expo_prenat_PELAGIE_Q2_temp200m_03062020.rds
│   │   ├── expo_prenat_PELAGIE_Q6_temp1km_03062020.rds
│   │   └── expo_prenat_PELAGIE_Q6_temp200m_03062020.rds
│   │
│   ├── Expo_SEPAGES_2020-11
│   │   │
│   │   ├── Average_exposure
│   │   │   ├── expo_prenat_SEPAGES_temp1km_1y_06112020.rds
│   │   │   └── expo_prenat_SEPAGES_temp200m_1y_06112020.rds
│   │   │
│   │   └── Exposure
│   │       ├── Temperature1km_matrices
│   │       │   ├── SEPAGES_temp_1km_dates_1y_04112020.rds
│   │       │   └── SEPAGES_temp_1km_tmean_1y_04112020.rds
│   │       ├── Temperature200m_matrices
│   │       │   ├── SEPAGES_temp_200m_dates_1y_04112020.rds
│   │       │   └── SEPAGES_temp_200m_tmean_1y_04112020.rds
│   │       └── Urbanization_matrices
│   │           ├── SEPAGES_urba_1y_04112020.rds
│   │           └── SEPAGES_urba_1y_dates_04112020.rds
│   │
│   ├── Expo_SEPAGES_2021-05
│   │   └── SEPAGES_NDVI_1y_04022021.rds
│   │
│   ├── NDVI_EDEN
│   │   └── NDVI_EDEN_29012021.rds
│   │
│   ├── NDVI_PELAGIE
│   │   ├── NDVI_PELAGIE_gro_29012021.rds
│   │   ├── NDVI_PELAGIE_Q2_29012021.rds
│   │   └── NDVI_PELAGIE_Q6_29012021.rds
│   │
│   ├── Temp1km_EDEN
│   │   ├── eden_temp_1km_matrices_dates_01062020.rds
│   │   └── eden_temp_1km_matrices_tmean_01062020.rds
│   │
│   ├── Temp1km_PELAGIE
│   │   ├── PELAGIE_gro_temp_1km_matrices_dates_01062020.rds
│   │   ├── PELAGIE_gro_temp_1km_matrices_tmean_01062020.rds
│   │   ├── PELAGIE_Q2_temp_1km_matrices_dates_01062020.rds
│   │   ├── PELAGIE_Q2_temp_1km_matrices_tmean_01062020.rds
│   │   ├── PELAGIE_Q6_temp_1km_matrices_dates_01062020.rds
│   │   └── PELAGIE_Q6_temp_1km_matrices_tmean_01062020.rds
│   │
│   │
│   ├── Temp200m_EDEN
│   │   ├── eden_temp_200m_matrices_dates_02062020.rds
│   │   └── eden_temp_200m_matrices_tmean_02062020.rds
│   │
│   ├── Temp200m_PELAGIE
│   │   ├── PELAGIE_gro_temp_200m_matrices_dates_02062020.rds
│   │   ├── PELAGIE_gro_temp_200m_matrices_tmean_02062020.rds
│   │   ├── PELAGIE_Q2_temp_200m_matrices_dates_02062020.rds
│   │   ├── PELAGIE_Q2_temp_200m_matrices_tmean_02062020.rds
│   │   ├── PELAGIE_Q6_temp_200m_matrices_dates_02062020.rds
│   │   └── PELAGIE_Q6_temp_200m_matrices_tmean_02062020.rds
│   │
│   ├── Urba_EDEN
│   │   ├── EDEN_urba_03062020.rds
│   │   └── EDEN_urba_dates_03062020.rds
│   │
│   └── Urba_PELAGIE
│       ├── PELAGIE_gro_urba_03062020.rds
│       ├── PELAGIE_gro_urba_dates_03062020.rds
│       ├── PELAGIE_Q2_urba_03062020.rds
│       ├── PELAGIE_Q2_urba_dates_03062020.rds
│       ├── PELAGIE_Q6_urba_03062020.rds
│       └── PELAGIE_Q6_urba_dates_03062020.rds
│
├── meteo
│   ├── donnees_region1_20000101_20001231.txt
│   ├── donnees_region1_20010101_20011231.txt
│   ├── donnees_region1_20020101_20021231.txt
│   ├── donnees_region2_20000101_20001231.txt
│   ├── donnees_region2_20010101_20011231.txt
│   ├── donnees_region2_20020101_20021231.txt
│   ├── donnees_region3_20000101_20001231.txt
│   ├── donnees_region3_20010101_20011231.txt
│   ├── donnees_region3_20020101_20021231.txt
│   ├── donnees_region3_20030101_20031231.txt
│   ├── donnees_region4_20090101_20091231.txt
│   ├── donnees_region4_20100101_20101231.txt
│   ├── donnees_region4_20110101_20111231.txt
│   ├── donnees_region4_20120101_20121231.txt
│   ├── donnees_region4_20130101_20131231.txt
│   ├── donnees_region4_20140101_20141231.txt
│   ├── donnees_region5_20000101_20001231.txt
│   ├── donnees_region5_20010101_20011231.txt
│   ├── donnees_region5_20020101_20021231.txt
│   ├── Q97_2000reg1.fic
│   ├── Q97_2000reg2.fic
│   ├── Q97_2000reg3.fic
│   ├── Q97_98reg5.fic
│   ├── Q99-2000reg5.fic
│   └── station_metadata_2021-05.csv
│
├── bdd_grossesse_v4_1.Rdata
├── go_eden_pelagie_2013_07_01.dta
├── go_sexe_update20210908.csv
└── SEP_EDEN_PEL_geocodes_delivery_centrap_01022021.csv
```

# fr_ta-preterm

Analysis of the relationship between exposure to ambient temperature (heat and cold) during pregnancy and preterm birth in the Sepages, EDEN, and Pelagie cohorts.

Hough, I., Rolland, M., Guilbert, A., Seyve, E., Heude, B., Slama, R., Lyon-Caen, S., Pin, I., Chevrier, C., Kloog, I., Lepeule, J. (2022). Early delivery following chronic and acute ambient temperature exposure: a comprehensive survival approach. International Journal of Epidemiology. [https://doi.org/10.1093/ije/dyac190](https://doi.org/10.1093/ije/dyac190)


## Quickstart

This project uses [renv](https://rstudio.github.io/renv/articles/renv.html) and [drake](https://docs.ropensci.org/drake/).

1. Configure the environment

```r
renv::restore()
```

2. Run the workflow

```r
source("makefile.R")
```

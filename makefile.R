# Workflow
# I. Hough & M. Rolland
# 2022-06-28

# Set default encoding for sourced files to UTF-8
# Otherwise sourced files are are assumed to be the system's default encoding (e.g. Windows-1252)
options(encoding = "UTF-8")

# Source all project elements ----
source("R/packages.R")
source("R/analysis_plan.R")
source("R/data_preparation.R")
source("R/ehf_preparation.R")
source("R/multivariate_model.R")
source("R/figures.R")
source("R/tables.R")

# Possible further sensitivity analyses:
# * Stratify by induced / spontaneous preterm birth
# * Stratify by late/moderate/very preterm

# # List and visualize outdated targets ----
# vis_drake_graph(plan, targets_only = TRUE)

# Predict next runtime ----
outdated(plan)
predict_runtime(plan)

# Run plan ----
make(plan, memory_strategy = "autoclean")
